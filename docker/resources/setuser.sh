#!/usr/bin/env bash

# (c) 2018, Andres Aquino <inbox@andresaquino.sh>
# This file is licensed under the BSD License version 3 or later. 
# See the LICENSE file.

# cnf: /usr/local/arte/etc/shell.setup/bash.rc : .bashrc
# cnf: /usr/local/arte/etc/shell.setup/bash.profile : .bash_profile
# cnf: /usr/local/arte/etc/tmux.profile : .tmux.conf
# cnf: /usr/local/arte/etc/tmux.profile : .tmux.conf

# Setup environment
echo "Setting appuser profile"
cp -r /usr/local/arte/paths.d ${HOME}

while read -r fConf
do
   # dirname & basename
   fName=${fConf%/*}
   fDir=${fConf##*/}

   # get final name
   uName=$(sed -n 's/^# NAME: \(.*\)/\1/p' ${fConf})

   # replicate
   cat ${fConf} > ${HOME}/${uName}

done < <(sed -n 's/^# cnf: \(.*\)/\1/p' $1)
cat /usr/local/arte/etc/shell.setup/bash.rc > ${HOME}/.bashrc
cat /usr/local/arte/etc/shell.setup/bash.profile > ${HOME}/.bash_profile
cp -r /usr/local/arte/etc/tmux.profile ${HOME}/.tmux.conf
cp -r /usr/local/arte/etc/default.profile ${HOME}/

# setting bashrc
if [[ -d /opt/vimzheimer/docker/scripts ]]; then
	echo "Setting func profile"
	cat /opt/vimzheimer/docker/scripts/bashrc.sh > ${HOME}/.bashrc
fi

