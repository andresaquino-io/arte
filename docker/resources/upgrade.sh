#!/usr/bin/env bash

# (c) 2018, Andres Aquino <inbox@andresaquino.sh>
# This file is licensed under the BSD License version 3 or later.
# See the LICENSE file.
#

#
# Using RetHat alike system
grep -q "centos" /etc/os-release
if [[ $? -eq 0 ]]; then
	# install vim8
	curl \
		-L https://copr.fedorainfracloud.org/coprs/mcepl/vim8/repo/epel-7/mcepl-vim8-epel-7.repo \
		-o /etc/yum.repos.d/mcepl-vim8-epel-7.repo
	
	yum update -y vim*

	# install tmux
	cd /tmp
	git clone https://github.com/tmux/tmux.git

	cd tmux
	git checkout 2.7
	sh autogen.sh
	sh configure && make && make install

fi

# install mdp
cd /tmp
git clone https://github.com/visit1985/mdp.git

cd mdp
make
make install

