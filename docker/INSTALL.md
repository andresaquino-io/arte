**Docker Management**

Build

```bash
$> docker build \
    --rm \
    --tag aelabs/arte .

```



Run
[ --volume . => path of the project }

```bash
$> docker run \
    --rm \
    --interactive \
    --tty \
    --hostname arte-dev-aelabs \
    --volume .:/usr/local/arte \
    --name arte-aelabs aelabs/arte
```



Run
[ running interactive ]

```bash
$> docker run \
    --rm \
    --interactive \
    --tty \
    --hostname arte-dev-aelabs \
    --name arte-aelabs \
    --user appuser \
    --entrypoint /bin/bash aelabs/arte
```



Run
[ interactive with current path as volume ]

```bash
$> docker run \
    --rm \
    --interactive \
    --tty \
    --hostname arte-dev-aelabs \
    --name arte-aelabs \
    --volume .:/usr/local/arte \
    --user appuser \
    --entrypoint /bin/bash aelabs/arte
```



Attach a bash process

```bash
$> docker exec \
    --interactive \
    --tty arte-aelabs "/bin/bash"
```



Ref
https://hub.docker.com/r/thinca/vim/

