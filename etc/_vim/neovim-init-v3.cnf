" VIM Setup // Tweak Your Vim As A Powerful IDE
" vim:ft=vim:
"
" URL
" https://levelup.gitconnected.com/tweak-your-vim-as-a-powerful-ide-fcea5f7eff9c
"
" How to install ?
"     * Copy vimrc.conf to ${HOME}/.config/nvim/init.vim
"     * Copy neovim-init.conf to ${HOME}/.vimrc
"
" Notes
"     1. use what ever you want for indenting, but use spaces for aligning
"     2. indent !
"     3. follow the best practices for coding
"
scriptencoding utf-8

" Be iMproved
if has('vim_starting')
  set nocompatible
endif

" Python enabled
let g:python_host_skip_check=1
let g:python_host_prog = '/usr/local/bin/python2'

let g:python3_host_skip_check=1
let g:python3_host_prog = '/usr/local/bin/python3'

let vimplug_exists=expand('~/.vim/autoload/plug.vim')

if !filereadable(vimplug_exists)
  if !executable("/usr/bin/curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  silent !\/usr/bin/curl -sfLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

" Required
call plug#begin(expand('~/.vim/plugged'))

" Packages
"--------------------------------------
" Colorschemes
Plug 'rafi/awesome-vim-colorschemes'

" A light and configurable statusline/tabline plugin for Vim
" https://github.com/itchyny/lightline.vim
Plug 'itchyny/lightline.vim'

" super simple vim plugin to show the list of buffers in the command bar
" https://github.com/mengelbrecht/lightline-bufferline
Plug 'mengelbrecht/lightline-bufferline'

" surrounding text objects with whatever you want (paranthesis, quotes, html tags...)
" Visual + S + ",',<p>,(,[
Plug 'tpope/vim-surround'

" easily search, substitute, coercion to camel case / snake case / dote case / title case...
" crs (to snake case)  crm (to mixed case), cru (to upper case), cr- (to dash case), and cr. (to dot case)
Plug 'tpope/vim-abolish'

" the . command can repeat whatever you want!
" http://vimcasts.org/episodes/creating-repeatable-mappings-with-repeat-vim/
Plug 'tpope/vim-repeat'

" comment automatically depending on the file you're in
" gcc
Plug 'tpope/vim-commentary'

" a Git wrapper so awesome, it should be illegal
" https://github.com/tpope/vim-fugitive
Plug 'tpope/vim-fugitive'

" Display the indention levels with thin vertical lines
Plug 'tpope/vim-pathogen'

" Allows toggling bookmarks per line, also quickfix window gives access to all bookmarks.
" https://github.com/MattesGroeger/vim-bookmarks
Plug 'MattesGroeger/vim-bookmarks'

" Auto Pairs // Insert or delete brackets, parens, quotes in pair.
" https://github.com/jiangmiao/auto-pairs
" Plug 'jiangmiao/auto-pairs'

" Insert mode auto-completion for quotes, parens, brackets, etc.
" https://github.com/Raimondi/delimitMate
Plug 'Raimondi/delimitMate'

" A Vim alignment plugin
" https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Match more stuff with % (html tag, LaTeX...)
Plug 'andymass/vim-matchup'

" vimscript for gist to post, read, update and delete gist of github
" https://github.com/mattn/gist-vim
Plug 'mattn/webapi-vim'
Plug 'mattn/gist-vim'

" IndentLine // Display the indention levels with thin vertical lines
" https://github.com/Yggdroot/indentLine
" Plug 'Yggdroot/indentLine'
"

" fzf // poweful fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
Plug 'junegunn/fzf.vim'

" Nerdtree //
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind']} | Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" IndexedSearch // display the result when searching
Plug 'henrik/vim-indexed-search'

" EditorConfig // EditorConfig plugin for Vim
" https://github.com/sgur/vim-editorconfig
Plug 'sgur/vim-editorconfig'

" SuperTab // Perform all your vim insert mode completions with Tab
" https://github.com/ervandew/supertab
Plug 'ervandew/supertab'

" Snipmate// Snippets are separated from the engine.
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Deoplete
Plug 'Shougo/deoplete.nvim'

" ALE
Plug 'dense-analysis/ale'

" GITGutter
Plug 'airblade/vim-gitgutter'

" Syntaxis
" https://github.com/Rican7/php-doc-modded
" Plug 'StanAngeloff/php.vim', {'for': 'php'}

" PHPDoc headers
" https://github.com/Rican7/php-doc-modded
" Plug 'Rican7/php-doc-modded'

" DevIcons
" https://github.com/ryanoasis/vim-devicons
Plug 'ryanoasis/vim-devicons'

"" end
call plug#end()

syntax on
filetype plugin on

" Fix backspace indent and show tab
set backspace=indent,eol,start
set listchars=tab:↦\ ,extends:#,nbsp:.,eol:¶,space:·
set showbreak=»
set pumheight=15

" Tabs. May be overriten by autocmd rules
set smartindent smartcase smarttab
set softtabstop=-1

" Searching
set hlsearch
set incsearch
set ignorecase
set iskeyword=a-z,A-Z,48-57,_

" swp files
set nobackup nowritebackup noswapfile noexpandtab

" behaviour
set hidden
set noconfirm nowrap noshowmode
set autowrite autoread autoindent
set modeline
set modelines=10

set foldmethod=indent
set nofoldenable
set number relativenumber
set diffopt+=vertical

" ==
" Alwasy display the tabline status
set showtabline=2
set laststatus=2

" ==
" Passing options
set title
set titleold="Terminal"
set titlestring=%F
set colorcolumn=120
set scrolloff=3

" go to last line
"autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

" update lightline
autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()

" show signs
autocmd BufRead,BufNewFile * set signcolumn=yes

" if NERDtree hide signs
autocmd FileType nerdtree set signcolumn=no

" toggle number line
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu | set rnu   | endif
:  autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu | set nornu | endif
:augroup END

" ==
" keyboad move between buffers previous | next | hide
nnoremap <C-l> :bnext<CR>
nnoremap <C-h> :bprev<CR>
nnoremap <C-i> :hide<CR>

"
if (has('nvim'))
  let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
endif

"
if (has("termguicolors") && $TERM_PROGRAM ==# 'iTerm.app')
  set termguicolors
endif

" ==
" Themes
set cursorline

if ($ITERM_PROFILE ==# 'asScreenshot')
   set background=light
   colorscheme OceanicNextLight

   "hi Normal       ctermbg=NONE guibg=NONE
   hi SignColumn   ctermbg=blue guibg=NONE
   hi LineNr       ctermfg=grey guifg=grey ctermbg=NONE guibg=NONE
   hi CursorLineNr ctermbg=NONE guibg=NONE ctermfg=178 guifg=#d7af00

   let g:bufferscheme = 'PaperColor'
else
   set background=dark
   colorscheme apprentice

   "hi Normal       ctermbg=NONE guibg=NONE
   hi SignColumn   ctermbg=235 guibg=#262626
   hi LineNr       ctermfg=grey guifg=grey ctermbg=NONE guibg=NONE
   hi CursorLineNr ctermbg=NONE guibg=NONE ctermfg=178 guifg=#d7af00

   let g:bufferscheme = 'oldHope'
endif

let g:gitgutter_set_sign_backgrounds = 0

" ==
" SNIPPETS
let g:snips_author='Aquino <aquino@cisacv.mx>'

" ==
" ALE
hi clear ALEErrorSign
hi clear ALEWarningSign

hi Error    ctermfg=204 ctermbg=NONE guifg=#ff5f87 guibg=NONE
hi Warning  ctermfg=178 ctermbg=NONE guifg=#D7AF00 guibg=NONE
hi ALEError ctermfg=204 guifg=#ff5f87 ctermbg=52 guibg=#5f0000 cterm=undercurl gui=undercurl
hi link ALEErrorSign    Error
hi link ALEWarningSign  Warning

" let g:ale_linters = {
"            \ 'python': ['pylint'],
"            \ 'javascript': ['eslint'],
"            \ 'go': ['gobuild', 'gofmt'],
"            \ 'rust': ['rls']
"            \}
" let g:ale_fixers = {
"            \ '*': ['remove_trailing_lines', 'trim_whitespace'],
"            \ 'python': ['autopep8'],
"            \ 'javascript': ['eslint'],
"            \ 'go': ['gofmt', 'goimports'],
"            \ 'rust': ['rustfmt']
let g:ale_fixers = {
   \ '*': ['remove_trailing_lines', 'trim_whitespace']
   \ }
let g:ale_fix_on_save = 1
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0
let g:ale_lint_on_insert_leave = 0
let g:ale_lint_on_enter = 0
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '!'
let g:ale_sign_warning = '≠'
let g:ale_php_phpcs_standard = 'PSR12'
nmap <silent> <C-g> <Plug>(ale_next_wrap)

"
" ==
set complete=.,w,b,u,i

"==
" EditorConfig Setup
let g:editorconfig_blacklist = { 'filetype': ['git.*', 'fugitive'], 'pattern': ['\.un~$'] }

" ==
" NERDTree Setup
nnoremap <C-o> :NERDTreeToggle<CR>
let g:NERDTreeWinPos = "right"
let g:NERDTreeQuitOnOpen = 1
let g:NERDTreeShowHidden = 1

" ==
" FZF Setup
function! FzfOmniFiles()
   let is_git = system('git stats')
   if v:shell_error
      :Files
   else
      :GitFiles
   endif
endfunction

"let g:fzf_command_prefix = 'Fzf'
nnoremap <C-b> :Buffers<CR>
nnoremap <C-g>g :Ag<CR>
nnoremap <C-p> :call FzfOmniFiles()<CR>

" ==
" LightLine setup
let g:lightline = {}

"
function! LightlineReadonly()
   return &readonly ? '' : ''
endfunction

"
function! LightlineFugitive()
   if exists('*fugitive#head')
      let branch = fugitive#head()
      return branch !=# '' ? '.branch' : ''
   endif
   return ''
endfunction

" Special 〈〉| ❰❱ |【  】|〔 〕|〚 〛|  ▛ ▜ | ▞ ▚ | ⎇  |  〉|  | ⠼ ⣃
let g:lightline = {
    \  'active': {
    \     'left':     [  [ 'mode', 'paste' ],
    \                    [ 'fugitive', 'readonly', 'modified' ] ],
    \     'right':    [  [ 'lineinfo', 'percent' ],
    \                    [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ],
    \                    [ 'fileencoding' ] ],
    \  },
    \  'inactive': {
    \     'left':     [  [ 'filename' ] ],
    \     'right':    [  [ 'percent' ] ],
    \  },
    \  'tabline': {
    \     'left':     [  [ 'buffers' ] ],
    \     'right':    [  [ 'fileformat', 'filetype' ] ],
    \  },
    \  'component': {
    \     'lineinfo': ' %3l:%-2v',
    \  },
    \  'component_function': {
    \     'readonly': 'LightlineReadonly',
    \     'fugitive': 'LightlineFugitive',
    \  },
    \  'component_expand': {
    \     'buffers': 'lightline#bufferline#buffers',
    \     'linter_checking': 'lightline#ale#checking',
    \     'linter_infos': 'lightline#ale#infos',
    \     'linter_warnings': 'lightline#ale#warnings',
    \     'linter_errors': 'lightline#ale#errors',
    \     'linter_ok': 'lightline#ale#ok',
    \  },
    \  'component_type': {
    \     'buffers': 'tabsel',
    \     'linter_checking': 'right',
    \     'linter_infos': 'right',
    \     'linter_warnings': 'warning',
    \     'linter_errors': 'error',
    \     'linter_ok': 'right',
    \  },
    \  'separator': {
    \     'left': '▛',
    \     'right': '▜'
    \  },
    \  'subseparator': {
    \     'left': '▞',
    \     'right': '▚'
    \  },
    \ 'colorscheme': g:bufferscheme,
\ }

let g:lightline#bufferline#unnamed = '[No Name]'
let g:lightline#bufferline#enable_devicons = 1
let g:lightline#bufferline#show_number = 1
let g:lightline#bufferline#shorten_path = 1
let g:lightline#bufferline#number_map = { 0: '⁰', 1: '¹', 2: '²', 3: '³', 4: '⁴', 5: '⁵', 6: '⁶', 7: '⁷', 8: '⁸', 9: '⁹' }

"==
" NVIM configuration
if has('mnvim')
    " Enable deoplete when InsertEnter.
    let g:deoplete#enable_at_startup = 0
    autocmd InsertEnter * call deoplete#enable()

    set belloff=""
    call deoplete#custom#source('_',  'max_menu_width', 0)
    call deoplete#custom#source('_',  'max_abbr_width', 0)
    call deoplete#custom#source('_',  'max_kind_width', 0)

    set hidden
    let g:LanguageClient_serverCommands = {
        \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
        \ 'go': ['~/.go/bin/gopls']
        \ }
endif
