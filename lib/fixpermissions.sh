#!/usr/bin/env bash
# 
# (c) 2018, Andrés Aquino <inbox@andresaquino.sh>
# This file is licensed under the BSD License version 3 or later.
# See the LICENSE file.
# 
 
cd /usr/local 
ln -sf /usr/local/arte/etc/bashrc . \
ln -sf /usr/local/arte/etc/unixrc . \
ln -sf /usr/local/arte/logo.d/default.info logo.info
 
find /usr/local/arte -type d -exec chmod a+rx {} \; \
   && find /usr/local/arte -type f -exec chmod a+r {} \;

